<?php

use Illuminate\Database\Seeder;

class forgetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('forget')->insert([
            
            'email' => str_random(10).'@gmail.com',
            
        ]);
    }
}
