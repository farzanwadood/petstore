<?php

use Illuminate\Database\Seeder;

class postaddTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('postadd')->insert([
            'subject' => str_random(10),
           'price' => str_random(10),
             'message' => str_random(10),
        ]);
    }
}
