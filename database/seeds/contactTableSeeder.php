<?php

use Illuminate\Database\Seeder;

class contactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('contact')->insert([
            'name' => str_random(10),
            'website' => str_random(10),
            'subject' => str_random(10),
            'email' => str_random(10).'@gmail.com',
           'message' => str_random(10),
        ]);
    }
}
