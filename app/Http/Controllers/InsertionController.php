<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Hash;
class InsertionController extends Controller
{

	public function index()
    {
        
    }
      public function signup(Request $request)
    {
        $v = Validator::make($request->all(),
    		['first_name'=>['required'],
    		'last_name'=>['required'],
    		'email'=>['required']
    		]);
    	if($v->fails())
    	{
    		//return back();

    		return redirect('signup')->withErrors($v)->withInput(); 

    	}
       /* $User= new user();
        $user->name='asd';
        $user->save();*/
        //dd('asd');
        $first_name=\Input::get('first_name');
        //dd($first_name);
        $last_name=\Input::get('last_name');
        $email=\Input::get('email');
        $password=\Input::get('password');
       // $remember_token=\Input::get('remember_token');
        $date=\Input::get('Y-m-d H:i:s');
        
        $data=array(
        "f_name" =>$first_name,
        "l_name" =>$last_name,
        "email"=>$email,
        "password"=>HASH::make($password),
      //  "remember_token"=>$remember_token,
        "created_at"=>$date,
        "updated_at"=>$date,
        );
        $id_email = DB::table('signup')->select('email')->where('email',$email)->get();
        
        if(count($id_email)==0)
        {
        DB::table('signup')->insert($data);
        return view('login');
}
		else
			{
					return redirect ('signup')->with("error","Email Already Used");
			}	
    }

       public function postadd(Request $request)
    {
    	$v = Validator::make($request->all(),
    		['subject'=>['required']
    		]);
    	if($v->fails())
    	{
    		//return back();
    		

    		return redirect('postadd')->withErrors($v)->withInput(); 
    	
    	 
    	}
        
       /* $User= new user();
        $user->name='asd';
        $user->save();*/
        //dd('asd');
        $subject=\Input::get('subject');
        //dd($first_name);
        $price=\Input::get('price');
        $message=\Input::get('message');
       
       // $remember_token=\Input::get('remember_token');
        $date=\Input::get('Y-m-d H:i:s');
        
        $data=array(
        "subject" =>$subject,
        "price" =>$price,
        "message"=>$message,
      //  "remember_token"=>$remember_token,
        "created_at"=>$date,
        "updated_at"=>$date,
        );
        DB::table('postadd')->insert($data);
        return view('myadd');

    }

    public function contact(Request $request)
    {

    	 $v = Validator::make($request->all(),
    		['name'=>['required'],
    		'subject'=>['required'],
    		'email'=>['required']
    		]);
    	if($v->fails())
    	{
    		return redirect('contact')->withErrors($v)->withInput(); 
    		//return back(); 
    	}
        $name=\Input::get('name');
        $email=\Input::get('email');
     	$website=\Input::get('website');
        $subject=\Input::get('subject');
        
        
        $message=\Input::get('message');
       
       // $remember_token=\Input::get('remember_token');
        $date=\Input::get('Y-m-d H:i:s');
        
        $data=array(
        "name" =>$name,
        "email" =>$email,
        "website" =>$website,
        "subject" =>$subject,
        "message"=>$message,
      //  "remember_token"=>$remember_token,
        "created_at"=>$date,
        "updated_at"=>$date,
        );
        DB::table('contact')->insert($data);
        return view('services');

    }
}
