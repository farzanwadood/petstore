<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Hash;
use Auth;

class LoginController extends Controller
{
      public function login(Request $request)
    {

    	$check = array("email"=>$request->email, "password"=>$request->password);
    	if(Auth::attempt($check))

    	//if(Auth::attempt(['email'=>$email, 'password'=>$password]))
    	{
    		//Auth::login();
    		return redirect()->intended('myprofile');

    	}
    	else
    	{

    		return redirect ('login')->with('error','Provid Valid user name Or Password');
    	}
        
    }
      public function profile()
    {

		 return view('profile');
    }


      public function logout()
    {
    		Auth::logout();
		 return redirect()->intended('logouted');
    }

      public function welcome()
    {

		 return view('welcome');
    }
}
