<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/','CustomerController@index');

Route::get('/home', ['as' => 'Home', function () {
       return view('welcome');

}]); 

Route::get('/about', ['as' => 'About', function () {
       return view('about');

}]); 

Route::get('/error', ['as' => 'Error', function () {
       return view('404');

}]); 

Route::get('/contact', ['as' => 'Contact', function () {
       return view('contact');

}]); 

Route::get('/forget', ['as' => 'Forget', function () {
       return view('forget');

}]); 

Route::get('/login', ['as' => 'Login', function () {
       return view('login');

}]); 

Route::get('/services', ['as' => 'Services', function () {
       return view('services');

}]); 

Route::get('/signup', ['as' => 'Signup', function () {
       return view('signUp');

}]); 


//Session and server side started



/* Route::get('/profile', ['as' => 'Profile', function () {
       return view('profile');

}]); */



/* Route::get('/logout', ['as' => 'Logout', function () {
       return view('welcome');

}]); */




 // DB CRUD start

 Route::post('/signup','InsertionController@signup');

 Route::post('/postadd','InsertionController@postadd');

 Route::post('/contact','InsertionController@contact');

 Route::post('/viewadd','FetchController@fetch');

 Route::post('/loginme','LoginController@login');
 Route::get('/logouted','LoginController@welcome');

 
///////////////////////////////////////////////////////////////////////////////

 Route::group(['middleware'=>'auth'],function(){
////////////////////////////////////////////////////////////////////////////////
 

 Route::get('/myprofile','LoginController@profile');

 Route::get('/logout', [
    'as' => 'Logout', 'uses' => 'LoginController@logout'
]);
  Route::get('/profile', ['as' => 'Profile', function () {
       return view('profile');

}]); 

  Route::get('/postadd', ['as' => 'PostAdd', function () {
       return view('postadd');

}]); 

 Route::get('/myadds', ['as' => 'MyAdds', function () {
       return view('myadd');

}]); 

  Route::get('/viewadds', ['as' => 'ViewAdds', function () {
       return view('viewadd');

}]);

});

 
