<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store! Your Online Pet Companion!</title>
{!! Html::style('theme/css/style.css') !!}
{!! Html::style('theme/css/hover.css') !!}
{!! Html::style('theme/css/bootstrap.min.css') !!}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed+Raleway' rel='stylesheet' type='text/css'> 
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
<style type="text/css">

</style>
</head>
<body>
<div class="header">                                                                                  
	<div class="wrap">                                                                                
	   <div class="header-top">	                                                                      
	        <div class="logo">                                                                        
				<a href="{{ route('Home') }}"> {!! Html::image('theme/images/logo.png') !!}</a>                                             
			</div>                                                                                    
	                   
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
	<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
			     <ul>
               <li ><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="has-sub"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               <li class="active"><a href="{{ route('Login') }}"><span>Login</span></a></li>
               
            
            </ul>                                                                                   
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                             
                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			 <div class="intro">
                <h2 class="title h2">Login</h2>
                
        	 </div>
        	
			 
				<div class="form-group">
   
<div class="signup-form">

@if(Session::has('error'))
        	 {{Session::get('error')}}
        	 @endif
	{!! Form::open(array('url'=>'loginme') )!!}
	<center>
	{!!  Form::text('email', '', array('class'=>'form_text','id'=>"email","placeholder"=>"Email Address")) !!}
    <br> <br>
    {!!  Form::password('password', '', array('class'=>'form_text','id'=>"password","placeholder"=>"Password")) !!}
	<br> <br>
	{!!  Form::submit('submit') !!}
	</center>
	{!! Form::close() !!}
</div> 


					 <!-- <div class="to">

                     	<input type="text" class="text" value="Your Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Email';}">
					 	<input type="text" class="text" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" style="margin-left: 10px">
					</div>  -->
		
	             <!--    <div>
	               		<a href="#" class="submit">Submit</a>
	                </div>

	                </div>
               </form> -->
            
            <Center><p><b>If you forget password Click blew</b></p></center>
			<Center><p><a href="{{ route('Forget') }}" class="submit">Forget Password</a></p></center><br><br><br>
            <Center><p><b>If you are new seller Click blew</b></p></center>
			<Center><p><a href="{{ route('Signup') }}" class="submit">SignUp</a></p></center><br><br><br>

		</div>
		</div>
	</div>
 </div>
 <!-- <div class="map">
	 <iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#666;text-align:left;font-size:12px"> </a></small>
 </div> -->
    <div class="footer">
	<div class="wrap">
		 <div class="copy">
			<p> � 2016 All rights Reserved | Design by Ghost</a></p>
		</div>
   <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
		<div class="clear"></div> 
    </div>
	<div class="footer-bot">
 		<a href="#toTop" class="toTop">&uarr;</a>
			<script type="text/javascript">
				$('.toTop ').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});
				$('.toBottom').click(function(){
					$('html,body').animate({scrollTop: $(document).height()}, 600);
					return false;
				});
			</script>
	</div>
	<div class="clear"></div>
  </div>

</body>

</html>

    	
    	
            