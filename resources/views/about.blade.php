<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store! Your Online Pet Companion!</title>
{!! Html::style('theme/css/style.css') !!}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'> 
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
<!--light-box-->
{!! Html::script('theme/js/jquery.lightbox.js') !!}

{!! Html::style('theme/css/lightbox.css') !!}
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>    
</head>
<body>
<div class="header">                                                                                  
	<div class="wrap">                                                                                
	   <div class="header-top">	                                                                      
	       <div class="logo">
                <a href="{{ route('Home') }}">
                {!! Html::image('theme/images/logo.png') !!}</a>
            </div>                                                                                  
		<!-- sign Up Button  -->
               <a href="{{ route('Signup') }}" class="linkWithWhiteColor">
               <button class="tryButton hvr-pulse-grow" type="submit">
			         Sign Up
			</button> 
			<!-- End Sign Up Button -->                                                                                   
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
		<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
	             <ul>
               <li ><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li class="active"><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="has-sub"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               <li class="last"><a href="{{ route('Login') }}"><span>Login</span></a></li>
               
            
            </ul>                                                                                   
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                              
</div>                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			  <div class="about-grids">
					<div class="about-grid">
						<h3>About PetShop</h3>
						<a href="#">{!! Html::image('theme/images/banner1.jpg') !!}</a>
						<span>"An animal's eyes have the power to speak a great language"<br>Martin Buber</span>

						<p>Pet Shop is a platform for your pets. Under License and Federal Law of Animal Welfare of 2016, A variety of animal supplies and pet accessories are also sold in our Pet Shop. All you have to do is simply Login/Signup into your account and Get Started</p>
						

						<a class="button1" href="#">Read More</a>
					</div>
					

					<div class="about-grid center-grid1">
						<h3>WHY CHOOSE US?</h3>
						<p>Have you got a Pet? Do you want to get a new one sitting at Home?</p>
						<label>In the USA and Canada, pet shops often offer both hygienic care (such as pet cleaning) and esthetic services (such as cat and dog grooming)</label>
						<p>We have chosen this website to fill up pet keeping niche. People around the world are indulging into love for the pets as its also becoming status symbol globally. It gives a quality time experience and a rejoice of being intact and self-actualization ability. Considering all these scenarios we come up with a system to reinforce this rising critique and provided with a portal for the business as well.</p>
						<label>"We are enlightening and contracting the space between the love of an animal and human" says Dr. Farzan Wadood (Co-Founder of Brains)</label>
						<p></p>
					</div>
					

					<div class="about-grid last-grid1">
						<h3>Our Clients</h3>
						<div class="about-team">
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic9.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>"Until one has loved an animal a part of one's soul remains unawakened." Amjad Sabri</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic10.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>"If having a soul means being able to feel love and loyalty and gratitude, then animals are better off than a lot of humans." Asad Iqbal</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic11.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>"Outside of a dog, a book is a man's best friend. Inside of a dog it's too dark to read." Groucho Marx</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic12.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>"I believe cats to be spirits come to earth. A cat, I am sure, could walk on a cloud without coming through." Jules Verne</p>
								</div>
								<div class="clear"> </div>
							</div>
						</div>
					</div>
					<div class="clear"> </div>
				</div>
			</div><br>
		</div>
	</div>



	<div class="content-middle">
		<div class="wrap">
			<h5 class="head">Our Team</h5>
		       <div class="middle-top">
				<div class="col_1_of_3 span_1_of_3">
					<div class="dc-head">
						<div class="dc-head-img">
							<a href="#">{!! Html::image('theme/images/pic.jpg') !!}</a>
						</div>
						<div class="dc-head-info">
							<h3>Asifa Naeem</h3>
							<span>Senior Web Designer</span>
						</div>
						<div class="clear"> </div>
						<div class="dc-profile">
							<p>"Pet stores are extremely popular in today’s society. In 2016, according to the American Pet Products Manufacturers Association, in the pet industry, live animal sales reached approximately $1.6 billion."</p>
							<button class="btn btn-6 btn-6a">Read More</button>
						</div>
					</div>
				</div>
				<div class="col_1_of_3 span_1_of_3">
					<div class="dc-head">
						<div class="dc-head-img">
							<a href="#">{!! Html::image('theme/images/pic1.jpg') !!}</a>
						</div>
						<div class="dc-head-info">
							<h3>Amna Jameel</h3>
							<span>Laravel Developer</span>
						</div>
						<div class="clear"> </div>
						<div class="dc-profile">
							<p>"It wasnt an easy task to put out the problems but that's sure it will benefit way too much for the people looking for love in pets"</p>
							<button class="btn btn-6 btn-6a">Read More</button>
						</div>
					</div>
				</div>
				<div class="col_1_of_3 span_1_of_3">
					<div class="dc-head">
						<div class="dc-head-img">
							<a href="#">{!! Html::image('theme/images/pic2.jpg') !!}</a>
						</div>
						<div class="dc-head-info">
							<h3>Natasha Bedingfield</h3>
							<span>Customer Support Team Manager</span>
						</div>
						<div class="clear"> </div>
						<div class="dc-profile">
							<p>"I see different type of people show there love and kindness for animals. This proves that nature has so much to grasp under a shadow"</p>
							<button class="btn btn-6 btn-6a">Read More</button>
						</div>
					</div>
				</div>
				<div class="clear"></div> 
		   </div>
		 </div>
	</div>
	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			<div class="middle-top">
				<h5 class="head">Our Gallery</h5>
				<div class="gallery">
					<ul>
						<li><a href="images/t-pic6.jpg" class="magnifier">{!! Html::image('theme/images/pic6.jpg') !!}<span></span></a>
							<div class="dc-head1">
								<h3>Kitty Pow</h3>
								<span>The Rodriger Dog</span>
						   </div></li>
						<li><a href="images/t-pic7.jpg" class="magnifier">{!! Html::image('theme/images/pic7.jpg') !!}<span></span></a> 
							<div class="dc-head1">
								<h3>Mathilda And her Cat</h3>
								<span>from SYDNEY</span>
						    </div></li>
						<li class="last"><a href="images/t-pic8.jpg" class="magnifier">{!! Html::image('theme/images/pic8.jpg') !!}<span></span></a>
							<div class="dc-head1">
								<h3>Rocketeer</h3>
								<span>Lahore, Paksitan</span>
						    </div></li>
						<div class="clear"> </div>
					</ul><br>
				</div>
				<div class="clear"></div> 
		   </div>
		</div>
	  </div>
	</div>
   </div>
	<div class="footer-box">
	     <div class="wrap">
	     	<h4 class="f-head">
				FORBES NEWS
			</h4>	
	    	<div id="slideshow">
			 	<div class="f-desc1">
					<p><center>Considered the best in the Forbes 2016 magazine as one of the essential top Site ranked in 2016 census.</center></p>
				</div>
				<div class="f-desc1">
			 		<p>Forbes is an American business magazine. Published bi-weekly, it features original articles on finance, industry, investing, and marketing topics. Forbes also reports on related subjects such as technology, communications, science, and law.</p>
				</div>
			</div>
		<script>
			$("#slideshow > div:gt(0)").hide();
			setInterval(function() { 
			  $('#slideshow > div:first')
			    .fadeOut(00)
			    .next()
			    .fadeIn(1000)
			    .end()
			    .appendTo('#slideshow');
			},  5000);
		</script>
		</div>
	</div>


  <div class="footer">
	<div class="wrap">
		 <div class="copy">
			<p> © 2016 All rights Reserved | Design by Ghost</a></p>
		</div>
   <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
		<div class="clear"></div> 
    </div>
	<div class="footer-bot">
 		<a href="#toTop" class="toTop">&uarr;</a>
			<script type="text/javascript">
				$('.toTop ').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});
				$('.toBottom').click(function(){
					$('html,body').animate({scrollTop: $(document).height()}, 600);
					return false;
				});
			</script>
	</div>
	<div class="clear"></div>
</div>
</body>
</html>

    	
    	
            