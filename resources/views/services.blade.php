<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store! Your Online Pet Companion!</title>
{!! Html::style('theme/css/style.css') !!}
{!! Html::style('theme/css/hover.css') !!}
{!! Html::style('theme/css/slider.css') !!}
{!! Html::style('theme/css/lightbox.css') !!}



<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed+Josefin+Sans&subset=latin'
 rel='stylesheet' type='text/css'>
<!--slider-->
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
{!! Html::script('theme/js/jquery.nivo.slider.js') !!}
<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
<!--light-box-->
{!! Html::script('theme/js/jquery.lightbox.js') !!}
    <script type="text/javascript">
        $(function() {
            $('.gallery a').lightBox();
        });
   </script>    
</head>
<body>
<div class="header">
    <div class="wrap">   
       <div class="header-top"> 
            <div class="logo">
                <a href="{{ route('Home') }}">
                {!! Html::image('theme/images/logo.png') !!}</a>
            </div>

            <!-- sign Up Button  -->
               <a href="{{ route('Signup') }}" class="linkWithWhiteColor">
               <button class="tryButton hvr-pulse-grow" type="submit">
                     Sign Up
            </button> 
            <!-- End Sign Up Button -->
            </a>		
			                                                                                 
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
	<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
		     <ul>
               <li ><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="active"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               <li class="last"><a href="{{ route('Login') }}"><span>Login</span></a></li>
               
            
            </ul>                                                                                    
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                             
</div>                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			 <div class="about-grids">
				      <div class="service-content">
							<h3>Welcome to PetShop Services!</h3>
							
							<p>We are serving as an Intermediary, we take care about the things of our users and clients so they wont feel out-rated!</p>
							<br><br>
							<ul> 
								<li><span>1.</span></li>
								<li><p><a href="#">PUSHING BOUNDARIES</a>Keeping track over the records, we added up an ease factor among our clients so they can fill their pet love appetite. Minimal Design showcase all the attributes that was required by our potential users. People, now gather at a place that serves them well in an eye of Business.</p></li>
								<div class="clear"> </div>
							</ul>
							
							<ul>
								<li><span>2.</span></li>
								<li><p><a href="#">WE TAKE CARE OF YOUR STUFF</a> We take care about your information and know you need to get things done undercover. Our system is developed by the brainiacs who implemented different encryptions so your data and information shall be kept safe and the Business shall run smoothly.</p></li>
								<div class="clear"> </div>
							</ul>
							<ul>
								<li><span>3.</span></li>
								<li><p><a href="#">WE LISTEN YOU 24/7</a>Our Enthusiast team is readily available to answer your potential question that should have been answered. Our Customer Service support remain intact with there work to provide you guides throughout your Business workout. </p></li>
								<div class="clear"> </div>
							
						</div>
						
						<div class="services-sidebar">
							<h3>Queries?</h3>
							 <ul>
							  	<li><a href="#">Online Services</a></li>
							  	
							  	<li><a href="#">Guides</a></li>
							  	<li><a href="#">FAQs</a></li>
							  	<li><a href="#">24/7 Customer Support</a></li>
					 		 </ul>
					 		 <div class="service-box"> </div>
					 		 <h3>ARCHIVES</h3>
					 		 <ul>
					 		 	<li><a href="#">JAN, 2016</a></li>
					 		 	<li><a href="#">FEB, 2016</a></li>
					 		 	<li><a href="#">MAR, 2016</a></li>
					 		 	<li><a href="#">APRIL, 2016</a></li>
					 		 </ul>
						</div>
						<div class="clear"> </div>
			         </div>
			
					</div>
	 		</div><br>
		</div>
	</div>


<div class="footer-box">
	     <div class="wrap">
	     	<h4 class="f-head">
				FORBES NEWS
			</h4>	
	    	<div id="slideshow">
			 	<div class="f-desc1">
					<p><center>Considered the best in the Forbes 2016 magazine as one of the essential top Site ranked in 2016 census.</center></p>
				</div>
				<div class="f-desc1">
			 		<p>Forbes is an American business magazine. Published bi-weekly, it features original articles on finance, industry, investing, and marketing topics. Forbes also reports on related subjects such as technology, communications, science, and law.</p>
				</div>
			</div>
		<script>
			$("#slideshow > div:gt(0)").hide();
			setInterval(function() { 
			  $('#slideshow > div:first')
			    .fadeOut(00)
			    .next()
			    .fadeIn(1000)
			    .end()
			    .appendTo('#slideshow');
			},  5000);
		</script>
		</div>
	</div>

	

  <div class="footer">
	<div class="wrap">
		 <div class="copy">
			<p> © 2016 All rights Reserved | Design by Ghost</a></p>
		</div>
   <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
		<div class="clear"></div> 
    </div>
	<div class="footer-bot">
 		<a href="#toTop" class="toTop">&uarr;</a>
			<script type="text/javascript">
				$('.toTop ').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});
				$('.toBottom').click(function(){
					$('html,body').animate({scrollTop: $(document).height()}, 600);
					return false;
				});
			</script>
	</div>
	<div class="clear"></div>
</div>
</body>
</html>