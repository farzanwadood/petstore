<!DOCTYPE HTML>
<html>
<head>
    <title>Pet-Store</title>
    {!! Html::style('theme/css/style.css') !!}
    {!! Html::style('theme/css/hover.css') !!}
    {!! Html::style('theme/css/slider.css') !!}
    {!! Html::style('theme/css/lightbox.css') !!}



    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed+Josefin+Sans&subset=latin'
          rel='stylesheet' type='text/css'>
    <!--slider-->
    {!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
    {!! Html::script('theme/js/jquery.nivo.slider.js') !!}
    <script type="text/javascript">
        $(window).load(function() {
            $('#slider').nivoSlider();
        });
    </script>
    <!--light-box-->
    {!! Html::script('theme/js/jquery.lightbox.js') !!}
    <script type="text/javascript">
        $(function() {
            $('.gallery a').lightBox();
        });
    </script>
</head>
<body>
<div class="header">
    <div class="wrap">
        <div class="header-top">
            <div class="logo">

                    {!! Html::image('theme/images/logo.png') !!}
            </div>

            <!-- sign Up Button  -->

                <!-- End Sign Up Button -->
            </a>

            <div class="clear"></div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="wrap">
            <div id="cssmenu">
                <ul>
                    <li ><a href="{{ route('Profile') }}"><span>Profile</span></a></li>
                    <li ><a href="{{ route('PostAdd') }}"><span>Post Add</span></a></li>
                    <li ><a href="{{ route('MyAdds') }}"><span>My Adds</span></a></li>
                    <li class="active"><a href="{{ route('ViewAdds') }}"><span>View Adds</span></a></li>
                    <li class="last"><a href="{{ route('Logout') }}"><span>Logout</span></a></li>

                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="main">
    <div class="top-box">
        <div class="wrap">
            <div class="content-top">
                <div class="about-grids">
                    <div class="service-content">

                    foreach ($postadds as $postadd) {
    echo $postadd->message;
}
                        
                        <h3>Naturex-club SERVED</h3>
                        <ul>
                            <li><span>1.</span></li>
                            <li><p><a href="#">MANUFACTURING &amp; INDUSTRIAL</a></p></li>
                            <div class="clear"> </div>
                        </ul>
                        <ul>
                            <li><span>2.</span></li>
                            <li><p><a href="#">FINANCIAL INSTITUTION</a>.</p></li>
                            <div class="clear"> </div>
                        </ul>
                        <ul>
                            <li><span>3.</span></li>
                            <li><p><a href="#">OFFICE BUILDING</a></p></li>
                            <div class="clear"> </div>
                        </ul>
                        <ul>
                            <li><span>4.</span></li>
                            <li><p><a href="#">RESIDENTIAL COMMUNITIES</a></p></li>
                            <div class="clear"> </div>
                        </ul>
                        <ul>
                            <li><span>5.</span></li>
                            <li><p><a href="#">RETAIL INDUSTRY</a></p></li>
                            <div class="clear"> </div>
                        </ul>
                        <ul>
                            <li><span>6.</span></li>
                            <li><p><a href="#">RETAIL INDUSTRY</a></p></li>
                            <div class="clear"> </div>
                        </ul>
                    </div>


                    <div class="clear"> </div>
                </div>

            </div>
        </div><br>
    </div>
</div>
<div class="footer-box">
    <div class="wrap">
        <h4 class="f-head">
            Lorem ipsum dolor
        </h4>
        <div id="slideshow">
            <div class="f-desc1">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse</p>
            </div>
            <div class="f-desc1">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse</p>
            </div>
        </div>
        <script>
            $("#slideshow > div:gt(0)").hide();
            setInterval(function() {
                $('#slideshow > div:first')
                        .fadeOut(00)
                        .next()
                        .fadeIn(500)
                        .end()
                        .appendTo('#slideshow');
            },  2000);
        </script>
    </div>
</div>
<div class="footer">
    <div class="wrap">
        <div class="copy">
            <p> © 2013 All rights Reserved | Design by <a href="http://petstore.com">Pet-Store</a></p>
        </div>
        <ul class="follow_icon">
            <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
            <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
            <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
            <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
        <div class="clear"></div>
    </div>
    <div class="footer-bot">
        <a href="#toTop" class="toTop">&uarr;</a>
        <script type="text/javascript">
            $('.toTop ').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
            $('.toBottom').click(function(){
                $('html,body').animate({scrollTop: $(document).height()}, 600);
                return false;
            });
        </script>
    </div>
    <div class="clear"></div>
</div>
</body>
</html>



