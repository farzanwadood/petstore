<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store! Your Online Pet Companion!</title>
{!! Html::style('theme/css/style.css') !!}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'> 
</head>
<body>
<div class="header">                                                                                  
	<div class="wrap">                                                                                
	   <div class="header-top">	                                                                      
	     <div class="logo">
                <a href="{{ route('Home') }}">
                {!! Html::image('theme/images/logo.png') !!}</a>
            </div>                                                                                   
			                                                                                   
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
	<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
             <ul>
               <li><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="has-sub"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               
               
            
            </ul>                                                                                     
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                             
</div>                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			 <div class="page-not-found">
				<h1>404</h1>
			 </div>
		  </div>
		</div>
	</div>
 </div>
  <div class="footer">
	<div class="wrap">
		 <div class="copy">
			<p> © 2016 All rights Reserved | Design by Ghost</a></p>
		</div>
   <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
		<div class="clear"></div> 
    </div>
 </div>
</body>
</html>

    	
    	
            