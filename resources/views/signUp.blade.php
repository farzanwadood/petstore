<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store</title>
{!! Html::style('theme/css/style.css') !!}
{!! Html::style('theme/css/hover.css') !!}
{!! Html::style('theme/css/bootstrap.min.css') !!}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed+Raleway' rel='stylesheet' type='text/css'> 
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
<style type="text/css">

</style>
</head>
<body>
<div class="header">                                                                                  
	<div class="wrap">                                                                                
	   <div class="header-top">	                                                                      
	         <div class="logo">
                <a href="{{ route('Home') }}">
                {!! Html::image('theme/images/logo.png') !!}</a>
            </div>                                                                                  
	                   
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
	<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
       <ul>
               <li><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="has-sub"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               <li class="last"><a href="{{ route('Login') }}"><span>Login</span></a></li>
               
            
            </ul>                                                                                 
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                             
</div>                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			 <div class="intro">
                <h2 class="title h2">Sign Up</h2>
                
        	 </div>
        	 <div class="signup-form">
        	 <div class ="alert alert-danger">
        	 
        	 @if (count($errors)>0)
        	 	<ul>
        	 	@foreach($errors->all() as $error)
        	 			<li>{{$error}}</li>
        	 	@endforeach
        	 	</ul>
        	 	</div>

        	 @endif

        	 @if(Session::has('error'))
        	 {{Session::get('error')}}
        	 @endif

	{!! Form::open(array('url'=>'signup') )!!}
	<center>
	{!!  Form::text('first_name', '', array('class'=>'form_text','id'=>"first_name","placeholder"=>"First Name")) !!}
	<br><br>	

	{!!  Form::text('last_name', '', array('class'=>'form_text','id'=>"last_name","placeholder"=>"Last Name")) !!}
	<br><br>
	{!!  Form::text('email', '', array('class'=>'form_text','id'=>"email","placeholder"=>"Email Address")) !!}
   <br><br>
    {!!  Form::password('password', '', array('class'=>'form_text','id'=>"password","placeholder"=>"Password")) !!}
    <br><br>

    
	
	{!!  Form::submit('submit') !!}
	</center>
	{!! Form::close() !!}
</div> 
			 <!-- <form method="post" action="contact-post.html">
					<div class="to">
                     	<input type="text" class="text" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}">
					 	<input type="text" class="text" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}" style="margin-left: 10px">
					</div>
					<div class="to">
                     	<input type="text" class="text" value="Your Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Email';}">
					 	<input type="text" class="text" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" style="margin-left: 10px">
					</div>
				
	                <div>
	               		<a href="#" class="submit">Submit</a>
	                </div>
               </form> -->
            </div>
		</div>
	</div>
 </div>

  <div class="footer">
	<div class="wrap">
		 <div class="copy">
			<p> © 2013 All rights Reserved | Design by <a href="http://petstore.com">Pet-Store</a></p>
		</div>
	        <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
		<div class="clear"></div> 
    </div>
	<div class="footer-bot">
 		<a href="#toTop" class="toTop">&uarr;</a>
			<script type="text/javascript">
				$('.toTop ').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 600);
					return false;
				});
				$('.toBottom').click(function(){
					$('html,body').animate({scrollTop: $(document).height()}, 600);
					return false;
				});
			</script>
	</div>
	<div class="clear"></div>
  </div>
</body>
</html>

    	
    	
            