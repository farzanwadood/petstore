<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store! Your Online Pet Companion!</title>
{!! Html::style('theme/css/style.css') !!}
{!! Html::style('theme/css/hover.css') !!}
{!! Html::style('theme/css/slider.css') !!}
{!! Html::style('theme/css/lightbox.css') !!}



<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed+Josefin+Sans&subset=latin'
 rel='stylesheet' type='text/css'>
<!--slider-->
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
{!! Html::script('theme/js/jquery.nivo.slider.js') !!}
<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
<!--light-box-->
{!! Html::script('theme/js/jquery.lightbox.js') !!}
    <script type="text/javascript">
        $(function() {
            $('.gallery a').lightBox();
        });
   </script>    
</head>
<body>
<div class="header">
    <div class="wrap">   
       <div class="header-top"> 
            <div class="logo">
                <a href="{{ route('Home') }}">
                {!! Html::image('theme/images/logo.png') !!}</a>
            </div>

            <!-- sign Up Button  -->
               <a href="{{ route('Signup') }}" class="linkWithWhiteColor">
               <button class="tryButton hvr-pulse-grow" type="submit">
                     Sign Up
            </button> 
            <!-- End Sign Up Button -->
            </a>
            <div class="clear"></div> 
        </div>
    </div>
    <div class="header-bottom">
      <div class="wrap">    
        <div id="cssmenu">
             <ul>
               <li class="active"><a href="{{ route('Home') }}"><span>Home</span></a></li>
               <li><a href="{{ route('About') }}"><span>About</span></a></li>
               <li class="has-sub"><a href="{{ route('Services') }}"><span>Services</span></a>
                  <ul>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Buy pet</span></a></li>
                     <li class="has-sub"><a href="{{ route('Services') }}"><span>Sell pet</span></a></li>
                  </ul>
               </li>
               <li class="last"><a href="{{ route('Contact') }}"><span>Contact</span></a></li>
               <li class="last"><a href="{{ route('Login') }}"><span>Login</span></a></li>
               
            
            </ul>
        </div>
        <div class="clear"></div> 
      </div>
   </div>
</div>
    <!------ Slider ------------>
         <div class="slider">
            <div class="slider-wrapper theme-default">
                <div id="slider" class="nivoSlider">
                {!! Html::image('theme/images/1.jpg') !!}
                {!! Html::image('theme/images/banner2.jpg') !!}
                {!! Html::image('theme/images/banner3.jpg') !!}
                {!! Html::image('theme/images/banner1.jpg') !!}
                {!! Html::image('theme/images/banner5.jpg') !!}
            
                </div>
           </div>
         </div>
  <!------End Slider ------------>
    
    <div class="main">
     <div class="top-box">
      
      <div class="wrap">
         <div class="content-top">
    
    <div class="section group">
                <div class="col_1_of_3 span_1_of_3">
                    <div class="grid_4">
                        <div class="box-1">
                        {!! Html::image('theme/images/pic3.jpg') !!}
                            
                            <div class="inside">
                                <h2 class="v1">Need A Pet :) </h2>
                                <p class="desc">You Landed at a Perfect place! Our Sellers has set up well! Go Register and start Browsing!</p>
                                <a href="#"><div class="clearfix"><span class="box-btn"></span></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <div class="grid_4">
                        <div class="box-1">
                        {!! Html::image('theme/images/pic4.jpg') !!}
                            
                            <div class="inside">
                                <h2 class="v1">Love Pets? </h2>
                                <p class="desc">We have got solution for your intimate love. You have cats to share? We have Oceanic Fishes for your Aquariums, they will love to spread colors at your hood.</p>
                                <a href="#"><div class="clearfix"><span class="box-btn"></span></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <div class="grid_4">
                        <div class="box-1">
                        {!! Html::image('theme/images/pic5.jpg') !!}
                            
                            <div class="inside">
                                <h2 class="v1">Have One!</h2>
                                <p class="desc">Its just too easy! Create a free account and you shall be right there! Make a Deal!</p>
                                <a href="#"><div class="clearfix"><span class="box-btn"></span></div></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div> 
            </div>
        </div>
      </div>
    </div>
   <div class="content-middle">
        <div class="wrap">
            <h5 class="head">Our Team</h5>
               <div class="middle-top">
                <div class="col_1_of_3 span_1_of_3">
                    <div class="dc-head">
                        <div class="dc-head-img">
                            <a href="#">{!! Html::image('theme/images/pic.jpg') !!}</a>
                        </div>
                        <div class="dc-head-info">
                            <h3>Asifa Naeem</h3>
                            <span>Senior Web Designer</span>
                        </div>
                        <div class="clear"> </div>
                        <div class="dc-profile">
                            <p>"Pet stores are extremely popular in today’s society. In 2016, according to the American Pet Products Manufacturers Association, in the pet industry, live animal sales reached approximately $1.6 billion."</p>
                            <button class="btn btn-6 btn-6a">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <div class="dc-head">
                        <div class="dc-head-img">
                            <a href="#">{!! Html::image('theme/images/pic1.jpg') !!}</a>
                        </div>
                        <div class="dc-head-info">
                            <h3>Amna Jameel</h3>
                            <span>Laravel Developer</span>
                        </div>
                        <div class="clear"> </div>
                        <div class="dc-profile">
                            <p>"It wasnt an easy task to put out the problems but that's sure it will benefit way too much for the people looking for love in pets"</p>
                            <button class="btn btn-6 btn-6a">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="col_1_of_3 span_1_of_3">
                    <div class="dc-head">
                        <div class="dc-head-img">
                            <a href="#">{!! Html::image('theme/images/pic2.jpg') !!}</a>
                        </div>
                        <div class="dc-head-info">
                            <h3>Natasha Bedingfield</h3>
                            <span>Customer Support Team Manager</span>
                        </div>
                        <div class="clear"> </div>
                        <div class="dc-profile">
                            <p>"I see different type of people show there love and kindness for animals. This proves that nature has so much to grasp under a shadow"</p>
                            <button class="btn btn-6 btn-6a">Read More</button>
                        </div>
                    </div>
                </div>
                <div class="clear"></div> 
           </div>
         </div>
    </div>
    

<div class="footer-box">
         <div class="wrap">
            <h4 class="f-head">
                FORBES NEWS
            </h4>   
            <div id="slideshow">
                <div class="f-desc1">
                    <p><center>Considered the best in the Forbes 2016 magazine as one of the essential top Site ranked in 2016 census.</center></p>
                </div>
                <div class="f-desc1">
                    <p>Forbes is an American business magazine. Published bi-weekly, it features original articles on finance, industry, investing, and marketing topics. Forbes also reports on related subjects such as technology, communications, science, and law.</p>
                </div>
            </div>
        <script>
            $("#slideshow > div:gt(0)").hide();
            setInterval(function() { 
              $('#slideshow > div:first')
                .fadeOut(00)
                .next()
                .fadeIn(1000)
                .end()
                .appendTo('#slideshow');
            },  5000);
        </script>
        </div>
    </div>

    

  <div class="footer">
    <div class="wrap">
         <div class="copy">
            <p> © 2016 All rights Reserved | Design by Ghost</a></p>
        </div>
   <ul class="follow_icon">
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/fb.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/tw.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/rss.png') !!}</a></li>
           <li><a href="#" style="opacity: 1;">{!! Html::image('theme/images/g+.png') !!}</a></li>
        </ul>
        <div class="clear"></div> 
    </div>
    <div class="footer-bot">
        <a href="#toTop" class="toTop">&uarr;</a>
            <script type="text/javascript">
                $('.toTop ').click(function(){
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
                $('.toBottom').click(function(){
                    $('html,body').animate({scrollTop: $(document).height()}, 600);
                    return false;
                });
            </script>
    </div>
    <div class="clear"></div>
</div>
</body>
</html>

        
        
            