<!DOCTYPE HTML>
<html>
<head>
<title>Pet-Store</title>
{!! Html::style('theme/css/style.css') !!}
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'> 
{!! Html::script('theme/js/jquery-1.9.0.min.js') !!}
<!--light-box-->
{!! Html::script('theme/js/jquery.lightbox.js') !!}

{!! Html::style('theme/css/lightbox.css') !!}
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>    
</head>
<body>
<div class="header">                                                                                  
	<div class="wrap">                                                                                
	   <div class="header-top">	                                                                      
	       <div class="logo">
              
                {!! Html::image('theme/images/logo.png') !!}
            </div>                                                                                  
		<!-- sign Up Button  -->
             
			<!-- End Sign Up Button -->                                                                                   
			<div class="clear"></div>                                                                 
	    </div>                                                                                        
	</div>                                                                                            
		<div class="header-bottom">                                                                       
	  <div class="wrap">	                                                                          
		<div id="cssmenu">                                                                            
	             <ul>
               <li class="active"><a href="{{ route('Profile') }}"><span>Profile</span></a></li>
               <li ><a href="{{ route('PostAdd') }}"><span>Post Add</span></a></li>
               <li ><a href="{{ route('MyAdds') }}"><span>My Adds</span></a></li>
               <li ><a href="{{ route('ViewAdds') }}"><span>View Adds</span></a></li>
 			<li class="last"><a href="{{ route('Logout') }}"><span>Logout</span></a></li>               
            
            </ul>                                                                                   
		</div>                                                                                        
		<div class="clear"></div>                                                                     
	  </div>                                                                                          
   </div>                                                                                              
</div>                                                                                                
 <div class="main">
 	<div class="top-box">
	  <div class="wrap">
		 <div class="content-top">
			  <div class="about-grids">
					<div class="about-grid">
						<h3>About Naturex</h3>
						<a href="#">{!! Html::image('theme/images/banner1.jpg') !!}</a>
						<span>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam.</span>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit.</p>
						<a class="button1" href="#">Read More</a>
					</div>
					<div class="about-grid center-grid1">
						<h3>WHY CHOOSE US?</h3>
						<p>Pellenn dimentum sed, commodo vitae, ornare sit amet,lit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.</p>
						<label>exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. orem ipsum dolor sit amet</label>
						<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetrornare sit amet, wisi. Aenean</p>
						<label>exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. orem ipsum dolor sit amet</label>
						<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetrornare sit amet, wisi. Aenean</p>
					</div>
					<div class="about-grid last-grid1">
						<h3>Our Clients</h3>
						<div class="about-team">
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic9.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic10.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic11.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#">{!! Html::image('theme/images/pic12.jpg') !!}</a>
								</div>
								<div class="about-team-right">
									<p>tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
								</div>
								<div class="clear"> </div>
							</div>
						</div>
					</div>
					<div class="clear"> </div>
				</div>
			</div><br>
		</div>
	</div>
</body>
</html>

    	
    	
            